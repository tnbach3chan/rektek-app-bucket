import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Set;
import org.junit.jupiter.api.Test;

class SearchJUnitTest {
	
	static ArrayList<String> appNames = new ArrayList<String>();
	
	static ArrayList<String> keywords = new ArrayList<String>();

	@Test
	void testAppSearch() {
		System.out.println("Search on app names:");
		appNames.add("qq");
		appNames.add("wechat");
		appNames.add("steam");
		appNames.add("minecraft");
		appNames.add("InvalidName");
		appNames.add("123456789");
		for (String appName : appNames) {
			System.out.println("\tApp Name: " + appName);
			Search.appSearch(appName);
			System.out.println();
		}
		System.out.println("Search on keywords:");
		keywords.add("we");
		keywords.add("mojang");
		keywords.add("e");
		keywords.add("invalid");
		keywords.add("chat");
		for (String keyword : keywords) {
			System.out.println("\tKeyword: " + keyword);
			Search.appSearch(keyword);
			System.out.println();
		}
	}
	
}
