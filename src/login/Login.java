package login;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import appInterface.appInterface;
import data.Customer;
import data.Data;

public class Login extends JFrame implements ActionListener, Runnable {
	private static final long serialVersionUID = 1L;
	protected JLabel lblPersonal, lblUserName, lblPassword, str;
	protected JTextField txtUserName;
	protected JPasswordField txtPassword;
	protected JTextField txtP;
	protected JButton Loginbtn, Registerbtn;
	protected static Thread thread = null;

	public static void main(String[] args) { // main method
		Data.init();
		Login Login1 = new Login();
		thread = new Thread(Login1);
		thread.start();
	}

	public Login() {
		super("AppBuckets");
		Component();
	}

	public void Component() {
		lblUserName = new JLabel("Account");
		lblPassword = new JLabel("Password");
		txtUserName = new JTextField(10);
		txtPassword = new JPasswordField(10);
		txtP = new JPasswordField(10);
		Loginbtn = new JButton("Login");
		Registerbtn = new JButton("Sign in");
		txtP.setText("");

		Loginbtn.addActionListener(this);
		Registerbtn.addActionListener(this);
		txtP.addActionListener(this);

		this.setLayout(new GridLayout(3, 3));

		this.add(lblUserName);
		this.add(txtUserName);
		this.add(lblPassword);
		this.add(txtPassword);

		this.add(Loginbtn);
		this.add(Registerbtn);
		setBounds(400, 300, 700, 400);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		String userName = txtUserName.getText().trim();
		String password = txtPassword.getText();
		int i;
		if (btn == Loginbtn) {
			if (txtUserName.getText().equals("") || txtUserName.getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this, "username can't be empty", "failed to login",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		for (i = 0; i < Data.customers.size(); i++) {
			if (Data.customers.get(i).getUserName().equals(userName)
					&& Data.customers.get(i).getPassword().equals(password)) {
				break;
			}
		}
		if (i < Data.customers.size()) {
			JOptionPane.showMessageDialog(this, "welcome" + userName, "login successful",
					JOptionPane.INFORMATION_MESSAGE);
			new appInterface();
		} else if (btn == Registerbtn) {
			this.dispose();
			new Register();
		}

	}

	@Override
	public void run() {
		// abstract

	}

	public static class Register extends JFrame implements ActionListener {
		private static final long serialVersionUID = 1L;
		protected JLabel lblUserName, lblPassword, lblConfirmedPassword;
		protected JTextField txtUserName;
		protected JPasswordField txtPassword, txtConfirmedPassword;
		protected JButton btnRegister, btnReset;

		public Register() {
			super("user register");
			init();
		}

		public void init() {
			lblUserName = new JLabel("username:");
			lblPassword = new JLabel("password");
			lblConfirmedPassword = new JLabel("confirm password");
			txtUserName = new JTextField(10);
			txtPassword = new JPasswordField(10);
			txtConfirmedPassword = new JPasswordField(10);

			btnReset = new JButton("reset");
			btnRegister = new JButton("sign in");

			btnReset.addActionListener(this);
			btnRegister.addActionListener(this);

			this.setLayout(new GridLayout(4, 3));
			this.add(lblUserName);
			this.add(txtUserName);
			this.add(lblPassword);
			this.add(txtPassword);
			this.add(lblConfirmedPassword);
			this.add(txtConfirmedPassword);
			this.add(btnRegister);
			this.add(btnReset);

			txtUserName.setFocusable(true);

			setBounds(500, 300, 600, 400);
			JFrame jFrame = new JFrame("login interface");
			jFrame.setLocationRelativeTo(null);
			this.setVisible(true);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton btn = (JButton) e.getSource();

			if (btn == btnRegister) {
				if (txtUserName.getText().equals("") || txtUserName.getText().trim().equals("")) {
					JOptionPane.showMessageDialog(this, "username cannot be empty", "fail to login",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (txtPassword.getText().equals("")) {
					JOptionPane.showMessageDialog(this, "please enter password", "fail to login",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (!txtConfirmedPassword.getText().equals(txtPassword.getText())) {
					JOptionPane.showMessageDialog(this, "confirm password should be the same", "fail to login",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				String userName = null;
				String password = null;
				int i;

				userName = txtUserName.getText().trim();
				password = txtPassword.getText();

				for (i = 0; i < Data.customers.size(); i++) {
					if (Data.customers.get(i).getUserName().equals(userName)) {
						break;
					}
				}
				if (i < Data.customers.size()) {
					JOptionPane.showMessageDialog(this, "username has been registered", "fail to register",
							JOptionPane.ERROR_MESSAGE);
				} else {
					Data.customers.add(new Customer(userName, password));
					JOptionPane.showMessageDialog(this, "hello, " + userName + " click\"OK\"to login",
							"register successful", JOptionPane.INFORMATION_MESSAGE);
					this.dispose();
					new Login();
				}
			} else if (btn == btnReset) {
				txtUserName.setText("");
				txtPassword.setText("");
				txtConfirmedPassword.setText("");
				txtUserName.setFocusable(true);
			}
		}

	}

}
