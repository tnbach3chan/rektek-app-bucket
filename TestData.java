import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestData {
    @Test
    public void TestData() {
        Data.init();
        assertEquals(1, Data.customers.size());
        Customer customer = Data.customers.get(0);
        assertEquals("stan", customer.getUserName());
        assertEquals("123456", customer.getPassword());


    }
}
